package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hazelcast.core.HazelcastInstance;
import java.util.Map;

@RestController
public class HomeController {

    @Autowired
    private HazelcastInstance hzInstance;


    @PostMapping(value = "/write-data")
    public String writeDataToHazelcast(@RequestParam String key, @RequestParam String value) {
        Map<String, String> hazelcastMap = hzInstance.getMap("my-map");
        for (int i = 0; i < 100000; i++) {
            hazelcastMap.put("ProductOffer" + i, "ProductOffer" + i + ", some product");
        }

        return "Data is stored.";
    }

    @PostMapping(value = "/update-data")
    public String updateDataToHazelcast(@RequestParam String key, @RequestParam String value) {
        Map<String, String> hazelcastMap = hzInstance.getMap("my-map");
        hazelcastMap.put(key, value);
        return "Data is stored.";
    }

    @GetMapping(value = "/read-data")
    public String readDataFromHazelcast(@RequestParam String key) {
        Map<String, String> hazelcastMap = hzInstance.getMap("my-map");
        return hazelcastMap.get(key);
    }

    @GetMapping(value = "/read-all-data")
    public Map<String, String> readAllDataFromHazelcast() {
        Map<String, String> hazelcastMap = hzInstance.getMap("my-map");
        return hzInstance.getMap("my-map");
    }

    @GetMapping(value = "/size")
    public Integer size() {
        Map<String, String> hazelcastMap = hzInstance.getMap("my-map");
        return hzInstance.getMap("my-map").size();
    }
}
