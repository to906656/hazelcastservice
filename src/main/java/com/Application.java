package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

@SpringBootApplication
public class Application {

    private static Config config;

    @Bean
    public HazelcastInstance hazelcastConfig() {
        Config config = new Config();
        JoinConfig joinConfig = config.getNetworkConfig().getJoin();
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getKubernetesConfig().setEnabled(true).setProperty("namespace", "cas")
                                                    .setProperty("service-name", "hazelcastservice")
                .setProperty("service-dns","hazelcastservice-cas.hazelcastservice-dev.svc.cluster.local");
        return Hazelcast.newHazelcastInstance(config);
    }


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
